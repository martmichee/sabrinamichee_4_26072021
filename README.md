# sabrinamichee_4_26072021

##  projet 4 seo pour l'ntreprise La Chouette

Le projet consiste à optimiser un site web existant. On travaille pour La chouette agence, une grande agence de web design basée à Lyon. On dois analyser l'état actuel de SEO, d'accessibilité et de performance du site fourni et relever puis appliquer 10 recommandations.



## Démarrage
Gitlab Pages: https://martmichee.gitlab.io/sabrinamichee_4_26072021/

## Technologie
Lighthouse - Outil pour évaluer la qualité des pages Web